# Changelog

### Sistemas Electrónicos Digitales - Registro de cambios en el proyecto

### [v2.0] - 2021/01/05
#### Añadido
- Proyectos auxiliares de **ADC** y **LCD**
#### Modificado
- Adición de un conversor ADC para la entrada de variable física
- Comunicación I2C con LCD 2004A
- Tratamiento de las conversiones ADC por *DMA*
- Inclusión de biblioteca de funciones `CMSIS DSP` de ARM
- Implementación de un *analizador de espectro* visible en LCD
### [v1.0] - 2020/12/15
#### Añadido
- Proyecto en blanco desde STM32CubeIDE
- No hay pines configurados
