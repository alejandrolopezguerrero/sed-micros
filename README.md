## Sistemas Electrónicos Digitales - Trabajo Microcontroladores
### Desarrollado por Alejandro López Guerrero 

### Dispositivos de interés
#### Microcontroladores y módulos
- STM32F411VE  
Microcontrolador principal del proyecto
- STM32F103C8T6  
Microcontrolador de sistema mínimo
- GPD2846A  
Decodificador MP3 desde SD a salida analógica
#### Módulos de comunicación
- ESP8266  
Módulo WIFI IOT  
[electrónica embajadores](https://www.electronicaembajadores.com/es/Productos/Detalle/LCWFNM3/modulos-electronicos/modulos-wifi/nodemcu-v3-modulo-wifi-esp8266-lua-wifi-ch340-fut6250)
- ZS040  
Módulo Bluetooth 4.0 Android IOS TTL BLE CC2540
- HC06  
Módulo Bluetooth 2.0 UART-SPI Genérico  
[electrónica embajadores](https://www.electronicaembajadores.com/es/Productos/Detalle/LCBTHT6/modulos-electronicos/modulos-bluetooth/hc-06-modulo-bluetooth-con-pines-fut-3373)
#### Pantallas
- OLED SSD1309
- LCD1602+I2C  
Pantalla alfanumérica de 2 filas x 16 columnas fondo azul + módulo adaptador I2C  
[electrónica embajadores](https://www.electronicaembajadores.com/es/Productos/Detalle/LCAL009/modulos-electronicos/modulos-lcd-alfanumericos/lcd-alfanumerico-16-x-2-azul-lcd1602-protocolo-i2c-4-pines)
- LCD2004-A+I2C  
Pantalla alfanumérica de 4 filas x 20 columnas fondo azul + módulo adaptador I2C  
[electrónica embajadores](https://www.electronicaembajadores.com/es/Productos/Detalle/LCAL012/modulos-electronicos/modulos-lcd-alfanumericos/lcd-alfanumerico-20x4-azul-lcd2004a-protocolo-i2c-4-pines-lcd2004)
- LCD12864  
Pantalla gráfica de 128 p x 64 p fondo azul   
[electrónica embajadores](https://www.electronicaembajadores.com/es/Productos/Detalle/LCGR005/modulos-electronicos/modulos-lcd-graficos/modulo-lcd-grafico-128-x-64-fondo-azul-lcd12864)
- EINK+SPI  
#### Displays
- Display 4 dígitos+I2C  
[electrónica embajadores](https://www.electronicaembajadores.com/es/Productos/Detalle/LCLE4719R/modulos-electronicos/modulos-led/display-4-digitos-led-7-segmentos-19-mm-i2c-rojo-fut4094-1)
### Enlaces de interés 
- [Controllerstech](https://controllerstech.com/) - Proyectos desarrollados con STM32
- [Fast Fourier Transform](https://www.nti-audio.com/en/support/know-how/fast-fourier-transform-fft) - Concepto práctico e ideas generales
- [Librería C FFT GNU](http://www.fftw.org/) - Open Source
- [CMSIS DSP Software Library](https://www.keil.com/pack/doc/CMSIS/DSP/html/group__RealFFT.html) - Librería de manejo de la transformada de Fourier en procesador ARM
