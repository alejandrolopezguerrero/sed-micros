/*
 * lcd.h
 *
 *  Created on: Dec 19, 2020
 *      Author: Alejandro López Guerrero - Javier Redondo Hernando
 */

#ifndef SRC_LCD_H_
#define SRC_LCD_H_

#include "stm32f4xx_hal.h"

void LCD_init (void);

void LCD_send_cmd (char cmd);

void LCD_send_data (char data);

void LCD_send_string (char *str);

void LCD_printf(char *str, int num);

void LCD_clear (void);

#endif /* SRC_LCD_H_ */
