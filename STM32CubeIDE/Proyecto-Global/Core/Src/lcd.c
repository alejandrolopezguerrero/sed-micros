/*
 * lcd.c
 *
 *  Created on: Dec 19, 2020
 *      Author: Alejandro L-opez Guerrero
 */

#include "lcd.h"

#define ADDRESS_LCD 0x4E //Dirección de LCD

extern I2C_HandleTypeDef hi2c1;

void LCD_init (void){

	//La siguiente secuencia de eventos ha sido obtenida de la hoja
	//de caracerísticas del dispositivo lcd

	// Interfaz de 4 bits
		HAL_Delay(50);
		LCD_send_cmd (0x30);
		HAL_Delay(5);
		LCD_send_cmd (0x30);
		HAL_Delay(1);
		LCD_send_cmd (0x30);
		HAL_Delay(10);
		LCD_send_cmd (0x20);
		HAL_Delay(10);


		LCD_send_cmd (0x28); // Function set --> DL=0 (4 bit mode), N = 1 (2 line display) F = 0 (5x8 characters)
		HAL_Delay(1);
		LCD_send_cmd (0x08); //Display on/off control --> D=0,C=0, B=0  ---> display off
		HAL_Delay(1);
		LCD_send_cmd (0x01);  // clear display
		HAL_Delay(2);
		LCD_send_cmd (0x06); //Entry mode set --> I/D = 1 (increment cursor) & S = 0 (no shift)
		HAL_Delay(1);
		LCD_send_cmd (0x0C);
		LCD_clear();
}

void LCD_send_cmd (char cmd){
	char data_u, data_l;
		uint8_t data_t[4];
		data_u = (cmd&0xf0);
		data_l = ((cmd<<4)&0xf0);
		data_t[0] = data_u|0x0C;  //en=1, rs=0
		data_t[1] = data_u|0x08;  //en=0, rs=0
		data_t[2] = data_l|0x0C;  //en=1, rs=0
		data_t[3] = data_l|0x08;  //en=0, rs=0
		HAL_I2C_Master_Transmit (&hi2c1, ADDRESS_LCD,(uint8_t *) data_t, 4, 100);
}

void LCD_send_data (char data){
	char data_u, data_l;
		uint8_t data_t[4];
		data_u = (data&0xf0);
		data_l = ((data<<4)&0xf0);
		data_t[0] = data_u|0x0D;  //en=1, rs=1
		data_t[1] = data_u|0x09;  //en=0, rs=1
		data_t[2] = data_l|0x0D;  //en=1, rs=1
		data_t[3] = data_l|0x09;  //en=0, rs=1
		HAL_I2C_Master_Transmit (&hi2c1, ADDRESS_LCD,(uint8_t *) data_t, 4, 100);
}

void LCD_send_string (char *str){
	while (*str) LCD_send_data (*str++);
}

void LCD_printf(char *str,int num){
	switch (num){
	case 0:
		LCD_send_cmd (0x80|0x00);
		LCD_send_string(str);
		break;
	case 1:
		LCD_send_cmd (0x80|0x40);
		LCD_send_string(str);
		break;
	case 2:
		LCD_send_cmd (0x80|0x14);
		LCD_send_string(str);
		break;
	case 3:
		LCD_send_cmd (0x80|0x54);
		LCD_send_string(str);
		break;
	default:
		break;
	}

}

void LCD_clear (void){
	LCD_send_cmd (0x00);
		for (int i=0; i<100; i++)
		{
			LCD_send_data (' ');
		}

}
