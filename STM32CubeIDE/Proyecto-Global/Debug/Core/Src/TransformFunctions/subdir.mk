################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Core/Src/TransformFunctions/TransformFunctions.c \
../Core/Src/TransformFunctions/TransformFunctionsF16.c \
../Core/Src/TransformFunctions/arm_bitreversal.c \
../Core/Src/TransformFunctions/arm_bitreversal2.c \
../Core/Src/TransformFunctions/arm_cfft_f16.c \
../Core/Src/TransformFunctions/arm_cfft_f32.c \
../Core/Src/TransformFunctions/arm_cfft_f64.c \
../Core/Src/TransformFunctions/arm_cfft_init_f16.c \
../Core/Src/TransformFunctions/arm_cfft_init_f32.c \
../Core/Src/TransformFunctions/arm_cfft_init_f64.c \
../Core/Src/TransformFunctions/arm_cfft_init_q15.c \
../Core/Src/TransformFunctions/arm_cfft_init_q31.c \
../Core/Src/TransformFunctions/arm_cfft_q15.c \
../Core/Src/TransformFunctions/arm_cfft_q31.c \
../Core/Src/TransformFunctions/arm_cfft_radix2_f16.c \
../Core/Src/TransformFunctions/arm_cfft_radix2_f32.c \
../Core/Src/TransformFunctions/arm_cfft_radix2_init_f16.c \
../Core/Src/TransformFunctions/arm_cfft_radix2_init_f32.c \
../Core/Src/TransformFunctions/arm_cfft_radix2_init_q15.c \
../Core/Src/TransformFunctions/arm_cfft_radix2_init_q31.c \
../Core/Src/TransformFunctions/arm_cfft_radix2_q15.c \
../Core/Src/TransformFunctions/arm_cfft_radix2_q31.c \
../Core/Src/TransformFunctions/arm_cfft_radix4_f16.c \
../Core/Src/TransformFunctions/arm_cfft_radix4_f32.c \
../Core/Src/TransformFunctions/arm_cfft_radix4_init_f16.c \
../Core/Src/TransformFunctions/arm_cfft_radix4_init_f32.c \
../Core/Src/TransformFunctions/arm_cfft_radix4_init_q15.c \
../Core/Src/TransformFunctions/arm_cfft_radix4_init_q31.c \
../Core/Src/TransformFunctions/arm_cfft_radix4_q15.c \
../Core/Src/TransformFunctions/arm_cfft_radix4_q31.c \
../Core/Src/TransformFunctions/arm_cfft_radix8_f16.c \
../Core/Src/TransformFunctions/arm_cfft_radix8_f32.c \
../Core/Src/TransformFunctions/arm_dct4_f32.c \
../Core/Src/TransformFunctions/arm_dct4_init_f32.c \
../Core/Src/TransformFunctions/arm_dct4_init_q15.c \
../Core/Src/TransformFunctions/arm_dct4_init_q31.c \
../Core/Src/TransformFunctions/arm_dct4_q15.c \
../Core/Src/TransformFunctions/arm_dct4_q31.c \
../Core/Src/TransformFunctions/arm_rfft_f32.c \
../Core/Src/TransformFunctions/arm_rfft_fast_f16.c \
../Core/Src/TransformFunctions/arm_rfft_fast_f32.c \
../Core/Src/TransformFunctions/arm_rfft_fast_f64.c \
../Core/Src/TransformFunctions/arm_rfft_fast_init_f16.c \
../Core/Src/TransformFunctions/arm_rfft_fast_init_f32.c \
../Core/Src/TransformFunctions/arm_rfft_fast_init_f64.c \
../Core/Src/TransformFunctions/arm_rfft_init_f32.c \
../Core/Src/TransformFunctions/arm_rfft_init_q15.c \
../Core/Src/TransformFunctions/arm_rfft_init_q31.c \
../Core/Src/TransformFunctions/arm_rfft_q15.c \
../Core/Src/TransformFunctions/arm_rfft_q31.c 

S_UPPER_SRCS += \
../Core/Src/TransformFunctions/arm_bitreversal2.S 

OBJS += \
./Core/Src/TransformFunctions/TransformFunctions.o \
./Core/Src/TransformFunctions/TransformFunctionsF16.o \
./Core/Src/TransformFunctions/arm_bitreversal.o \
./Core/Src/TransformFunctions/arm_bitreversal2.o \
./Core/Src/TransformFunctions/arm_cfft_f16.o \
./Core/Src/TransformFunctions/arm_cfft_f32.o \
./Core/Src/TransformFunctions/arm_cfft_f64.o \
./Core/Src/TransformFunctions/arm_cfft_init_f16.o \
./Core/Src/TransformFunctions/arm_cfft_init_f32.o \
./Core/Src/TransformFunctions/arm_cfft_init_f64.o \
./Core/Src/TransformFunctions/arm_cfft_init_q15.o \
./Core/Src/TransformFunctions/arm_cfft_init_q31.o \
./Core/Src/TransformFunctions/arm_cfft_q15.o \
./Core/Src/TransformFunctions/arm_cfft_q31.o \
./Core/Src/TransformFunctions/arm_cfft_radix2_f16.o \
./Core/Src/TransformFunctions/arm_cfft_radix2_f32.o \
./Core/Src/TransformFunctions/arm_cfft_radix2_init_f16.o \
./Core/Src/TransformFunctions/arm_cfft_radix2_init_f32.o \
./Core/Src/TransformFunctions/arm_cfft_radix2_init_q15.o \
./Core/Src/TransformFunctions/arm_cfft_radix2_init_q31.o \
./Core/Src/TransformFunctions/arm_cfft_radix2_q15.o \
./Core/Src/TransformFunctions/arm_cfft_radix2_q31.o \
./Core/Src/TransformFunctions/arm_cfft_radix4_f16.o \
./Core/Src/TransformFunctions/arm_cfft_radix4_f32.o \
./Core/Src/TransformFunctions/arm_cfft_radix4_init_f16.o \
./Core/Src/TransformFunctions/arm_cfft_radix4_init_f32.o \
./Core/Src/TransformFunctions/arm_cfft_radix4_init_q15.o \
./Core/Src/TransformFunctions/arm_cfft_radix4_init_q31.o \
./Core/Src/TransformFunctions/arm_cfft_radix4_q15.o \
./Core/Src/TransformFunctions/arm_cfft_radix4_q31.o \
./Core/Src/TransformFunctions/arm_cfft_radix8_f16.o \
./Core/Src/TransformFunctions/arm_cfft_radix8_f32.o \
./Core/Src/TransformFunctions/arm_dct4_f32.o \
./Core/Src/TransformFunctions/arm_dct4_init_f32.o \
./Core/Src/TransformFunctions/arm_dct4_init_q15.o \
./Core/Src/TransformFunctions/arm_dct4_init_q31.o \
./Core/Src/TransformFunctions/arm_dct4_q15.o \
./Core/Src/TransformFunctions/arm_dct4_q31.o \
./Core/Src/TransformFunctions/arm_rfft_f32.o \
./Core/Src/TransformFunctions/arm_rfft_fast_f16.o \
./Core/Src/TransformFunctions/arm_rfft_fast_f32.o \
./Core/Src/TransformFunctions/arm_rfft_fast_f64.o \
./Core/Src/TransformFunctions/arm_rfft_fast_init_f16.o \
./Core/Src/TransformFunctions/arm_rfft_fast_init_f32.o \
./Core/Src/TransformFunctions/arm_rfft_fast_init_f64.o \
./Core/Src/TransformFunctions/arm_rfft_init_f32.o \
./Core/Src/TransformFunctions/arm_rfft_init_q15.o \
./Core/Src/TransformFunctions/arm_rfft_init_q31.o \
./Core/Src/TransformFunctions/arm_rfft_q15.o \
./Core/Src/TransformFunctions/arm_rfft_q31.o 

S_UPPER_DEPS += \
./Core/Src/TransformFunctions/arm_bitreversal2.d 

C_DEPS += \
./Core/Src/TransformFunctions/TransformFunctions.d \
./Core/Src/TransformFunctions/TransformFunctionsF16.d \
./Core/Src/TransformFunctions/arm_bitreversal.d \
./Core/Src/TransformFunctions/arm_bitreversal2.d \
./Core/Src/TransformFunctions/arm_cfft_f16.d \
./Core/Src/TransformFunctions/arm_cfft_f32.d \
./Core/Src/TransformFunctions/arm_cfft_f64.d \
./Core/Src/TransformFunctions/arm_cfft_init_f16.d \
./Core/Src/TransformFunctions/arm_cfft_init_f32.d \
./Core/Src/TransformFunctions/arm_cfft_init_f64.d \
./Core/Src/TransformFunctions/arm_cfft_init_q15.d \
./Core/Src/TransformFunctions/arm_cfft_init_q31.d \
./Core/Src/TransformFunctions/arm_cfft_q15.d \
./Core/Src/TransformFunctions/arm_cfft_q31.d \
./Core/Src/TransformFunctions/arm_cfft_radix2_f16.d \
./Core/Src/TransformFunctions/arm_cfft_radix2_f32.d \
./Core/Src/TransformFunctions/arm_cfft_radix2_init_f16.d \
./Core/Src/TransformFunctions/arm_cfft_radix2_init_f32.d \
./Core/Src/TransformFunctions/arm_cfft_radix2_init_q15.d \
./Core/Src/TransformFunctions/arm_cfft_radix2_init_q31.d \
./Core/Src/TransformFunctions/arm_cfft_radix2_q15.d \
./Core/Src/TransformFunctions/arm_cfft_radix2_q31.d \
./Core/Src/TransformFunctions/arm_cfft_radix4_f16.d \
./Core/Src/TransformFunctions/arm_cfft_radix4_f32.d \
./Core/Src/TransformFunctions/arm_cfft_radix4_init_f16.d \
./Core/Src/TransformFunctions/arm_cfft_radix4_init_f32.d \
./Core/Src/TransformFunctions/arm_cfft_radix4_init_q15.d \
./Core/Src/TransformFunctions/arm_cfft_radix4_init_q31.d \
./Core/Src/TransformFunctions/arm_cfft_radix4_q15.d \
./Core/Src/TransformFunctions/arm_cfft_radix4_q31.d \
./Core/Src/TransformFunctions/arm_cfft_radix8_f16.d \
./Core/Src/TransformFunctions/arm_cfft_radix8_f32.d \
./Core/Src/TransformFunctions/arm_dct4_f32.d \
./Core/Src/TransformFunctions/arm_dct4_init_f32.d \
./Core/Src/TransformFunctions/arm_dct4_init_q15.d \
./Core/Src/TransformFunctions/arm_dct4_init_q31.d \
./Core/Src/TransformFunctions/arm_dct4_q15.d \
./Core/Src/TransformFunctions/arm_dct4_q31.d \
./Core/Src/TransformFunctions/arm_rfft_f32.d \
./Core/Src/TransformFunctions/arm_rfft_fast_f16.d \
./Core/Src/TransformFunctions/arm_rfft_fast_f32.d \
./Core/Src/TransformFunctions/arm_rfft_fast_f64.d \
./Core/Src/TransformFunctions/arm_rfft_fast_init_f16.d \
./Core/Src/TransformFunctions/arm_rfft_fast_init_f32.d \
./Core/Src/TransformFunctions/arm_rfft_fast_init_f64.d \
./Core/Src/TransformFunctions/arm_rfft_init_f32.d \
./Core/Src/TransformFunctions/arm_rfft_init_q15.d \
./Core/Src/TransformFunctions/arm_rfft_init_q31.d \
./Core/Src/TransformFunctions/arm_rfft_q15.d \
./Core/Src/TransformFunctions/arm_rfft_q31.d 


# Each subdirectory must supply rules for building sources it contributes
Core/Src/TransformFunctions/TransformFunctions.o: ../Core/Src/TransformFunctions/TransformFunctions.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F411xE -DDEBUG -c -I../Core/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../Drivers/CMSIS/Include -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Core/Src/TransformFunctions/TransformFunctions.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Core/Src/TransformFunctions/TransformFunctionsF16.o: ../Core/Src/TransformFunctions/TransformFunctionsF16.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F411xE -DDEBUG -c -I../Core/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../Drivers/CMSIS/Include -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Core/Src/TransformFunctions/TransformFunctionsF16.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Core/Src/TransformFunctions/arm_bitreversal.o: ../Core/Src/TransformFunctions/arm_bitreversal.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F411xE -DDEBUG -c -I../Core/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../Drivers/CMSIS/Include -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Core/Src/TransformFunctions/arm_bitreversal.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Core/Src/TransformFunctions/arm_bitreversal2.o: ../Core/Src/TransformFunctions/arm_bitreversal2.S
	arm-none-eabi-gcc -mcpu=cortex-m4 -g3 -c -x assembler-with-cpp -MMD -MP -MF"Core/Src/TransformFunctions/arm_bitreversal2.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@" "$<"
Core/Src/TransformFunctions/arm_bitreversal2.o: ../Core/Src/TransformFunctions/arm_bitreversal2.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F411xE -DDEBUG -c -I../Core/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../Drivers/CMSIS/Include -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Core/Src/TransformFunctions/arm_bitreversal2.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Core/Src/TransformFunctions/arm_cfft_f16.o: ../Core/Src/TransformFunctions/arm_cfft_f16.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F411xE -DDEBUG -c -I../Core/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../Drivers/CMSIS/Include -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Core/Src/TransformFunctions/arm_cfft_f16.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Core/Src/TransformFunctions/arm_cfft_f32.o: ../Core/Src/TransformFunctions/arm_cfft_f32.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F411xE -DDEBUG -c -I../Core/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../Drivers/CMSIS/Include -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Core/Src/TransformFunctions/arm_cfft_f32.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Core/Src/TransformFunctions/arm_cfft_f64.o: ../Core/Src/TransformFunctions/arm_cfft_f64.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F411xE -DDEBUG -c -I../Core/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../Drivers/CMSIS/Include -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Core/Src/TransformFunctions/arm_cfft_f64.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Core/Src/TransformFunctions/arm_cfft_init_f16.o: ../Core/Src/TransformFunctions/arm_cfft_init_f16.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F411xE -DDEBUG -c -I../Core/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../Drivers/CMSIS/Include -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Core/Src/TransformFunctions/arm_cfft_init_f16.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Core/Src/TransformFunctions/arm_cfft_init_f32.o: ../Core/Src/TransformFunctions/arm_cfft_init_f32.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F411xE -DDEBUG -c -I../Core/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../Drivers/CMSIS/Include -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Core/Src/TransformFunctions/arm_cfft_init_f32.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Core/Src/TransformFunctions/arm_cfft_init_f64.o: ../Core/Src/TransformFunctions/arm_cfft_init_f64.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F411xE -DDEBUG -c -I../Core/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../Drivers/CMSIS/Include -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Core/Src/TransformFunctions/arm_cfft_init_f64.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Core/Src/TransformFunctions/arm_cfft_init_q15.o: ../Core/Src/TransformFunctions/arm_cfft_init_q15.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F411xE -DDEBUG -c -I../Core/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../Drivers/CMSIS/Include -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Core/Src/TransformFunctions/arm_cfft_init_q15.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Core/Src/TransformFunctions/arm_cfft_init_q31.o: ../Core/Src/TransformFunctions/arm_cfft_init_q31.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F411xE -DDEBUG -c -I../Core/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../Drivers/CMSIS/Include -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Core/Src/TransformFunctions/arm_cfft_init_q31.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Core/Src/TransformFunctions/arm_cfft_q15.o: ../Core/Src/TransformFunctions/arm_cfft_q15.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F411xE -DDEBUG -c -I../Core/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../Drivers/CMSIS/Include -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Core/Src/TransformFunctions/arm_cfft_q15.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Core/Src/TransformFunctions/arm_cfft_q31.o: ../Core/Src/TransformFunctions/arm_cfft_q31.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F411xE -DDEBUG -c -I../Core/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../Drivers/CMSIS/Include -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Core/Src/TransformFunctions/arm_cfft_q31.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Core/Src/TransformFunctions/arm_cfft_radix2_f16.o: ../Core/Src/TransformFunctions/arm_cfft_radix2_f16.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F411xE -DDEBUG -c -I../Core/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../Drivers/CMSIS/Include -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Core/Src/TransformFunctions/arm_cfft_radix2_f16.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Core/Src/TransformFunctions/arm_cfft_radix2_f32.o: ../Core/Src/TransformFunctions/arm_cfft_radix2_f32.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F411xE -DDEBUG -c -I../Core/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../Drivers/CMSIS/Include -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Core/Src/TransformFunctions/arm_cfft_radix2_f32.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Core/Src/TransformFunctions/arm_cfft_radix2_init_f16.o: ../Core/Src/TransformFunctions/arm_cfft_radix2_init_f16.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F411xE -DDEBUG -c -I../Core/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../Drivers/CMSIS/Include -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Core/Src/TransformFunctions/arm_cfft_radix2_init_f16.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Core/Src/TransformFunctions/arm_cfft_radix2_init_f32.o: ../Core/Src/TransformFunctions/arm_cfft_radix2_init_f32.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F411xE -DDEBUG -c -I../Core/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../Drivers/CMSIS/Include -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Core/Src/TransformFunctions/arm_cfft_radix2_init_f32.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Core/Src/TransformFunctions/arm_cfft_radix2_init_q15.o: ../Core/Src/TransformFunctions/arm_cfft_radix2_init_q15.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F411xE -DDEBUG -c -I../Core/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../Drivers/CMSIS/Include -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Core/Src/TransformFunctions/arm_cfft_radix2_init_q15.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Core/Src/TransformFunctions/arm_cfft_radix2_init_q31.o: ../Core/Src/TransformFunctions/arm_cfft_radix2_init_q31.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F411xE -DDEBUG -c -I../Core/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../Drivers/CMSIS/Include -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Core/Src/TransformFunctions/arm_cfft_radix2_init_q31.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Core/Src/TransformFunctions/arm_cfft_radix2_q15.o: ../Core/Src/TransformFunctions/arm_cfft_radix2_q15.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F411xE -DDEBUG -c -I../Core/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../Drivers/CMSIS/Include -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Core/Src/TransformFunctions/arm_cfft_radix2_q15.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Core/Src/TransformFunctions/arm_cfft_radix2_q31.o: ../Core/Src/TransformFunctions/arm_cfft_radix2_q31.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F411xE -DDEBUG -c -I../Core/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../Drivers/CMSIS/Include -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Core/Src/TransformFunctions/arm_cfft_radix2_q31.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Core/Src/TransformFunctions/arm_cfft_radix4_f16.o: ../Core/Src/TransformFunctions/arm_cfft_radix4_f16.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F411xE -DDEBUG -c -I../Core/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../Drivers/CMSIS/Include -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Core/Src/TransformFunctions/arm_cfft_radix4_f16.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Core/Src/TransformFunctions/arm_cfft_radix4_f32.o: ../Core/Src/TransformFunctions/arm_cfft_radix4_f32.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F411xE -DDEBUG -c -I../Core/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../Drivers/CMSIS/Include -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Core/Src/TransformFunctions/arm_cfft_radix4_f32.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Core/Src/TransformFunctions/arm_cfft_radix4_init_f16.o: ../Core/Src/TransformFunctions/arm_cfft_radix4_init_f16.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F411xE -DDEBUG -c -I../Core/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../Drivers/CMSIS/Include -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Core/Src/TransformFunctions/arm_cfft_radix4_init_f16.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Core/Src/TransformFunctions/arm_cfft_radix4_init_f32.o: ../Core/Src/TransformFunctions/arm_cfft_radix4_init_f32.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F411xE -DDEBUG -c -I../Core/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../Drivers/CMSIS/Include -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Core/Src/TransformFunctions/arm_cfft_radix4_init_f32.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Core/Src/TransformFunctions/arm_cfft_radix4_init_q15.o: ../Core/Src/TransformFunctions/arm_cfft_radix4_init_q15.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F411xE -DDEBUG -c -I../Core/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../Drivers/CMSIS/Include -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Core/Src/TransformFunctions/arm_cfft_radix4_init_q15.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Core/Src/TransformFunctions/arm_cfft_radix4_init_q31.o: ../Core/Src/TransformFunctions/arm_cfft_radix4_init_q31.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F411xE -DDEBUG -c -I../Core/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../Drivers/CMSIS/Include -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Core/Src/TransformFunctions/arm_cfft_radix4_init_q31.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Core/Src/TransformFunctions/arm_cfft_radix4_q15.o: ../Core/Src/TransformFunctions/arm_cfft_radix4_q15.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F411xE -DDEBUG -c -I../Core/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../Drivers/CMSIS/Include -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Core/Src/TransformFunctions/arm_cfft_radix4_q15.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Core/Src/TransformFunctions/arm_cfft_radix4_q31.o: ../Core/Src/TransformFunctions/arm_cfft_radix4_q31.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F411xE -DDEBUG -c -I../Core/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../Drivers/CMSIS/Include -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Core/Src/TransformFunctions/arm_cfft_radix4_q31.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Core/Src/TransformFunctions/arm_cfft_radix8_f16.o: ../Core/Src/TransformFunctions/arm_cfft_radix8_f16.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F411xE -DDEBUG -c -I../Core/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../Drivers/CMSIS/Include -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Core/Src/TransformFunctions/arm_cfft_radix8_f16.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Core/Src/TransformFunctions/arm_cfft_radix8_f32.o: ../Core/Src/TransformFunctions/arm_cfft_radix8_f32.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F411xE -DDEBUG -c -I../Core/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../Drivers/CMSIS/Include -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Core/Src/TransformFunctions/arm_cfft_radix8_f32.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Core/Src/TransformFunctions/arm_dct4_f32.o: ../Core/Src/TransformFunctions/arm_dct4_f32.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F411xE -DDEBUG -c -I../Core/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../Drivers/CMSIS/Include -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Core/Src/TransformFunctions/arm_dct4_f32.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Core/Src/TransformFunctions/arm_dct4_init_f32.o: ../Core/Src/TransformFunctions/arm_dct4_init_f32.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F411xE -DDEBUG -c -I../Core/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../Drivers/CMSIS/Include -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Core/Src/TransformFunctions/arm_dct4_init_f32.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Core/Src/TransformFunctions/arm_dct4_init_q15.o: ../Core/Src/TransformFunctions/arm_dct4_init_q15.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F411xE -DDEBUG -c -I../Core/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../Drivers/CMSIS/Include -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Core/Src/TransformFunctions/arm_dct4_init_q15.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Core/Src/TransformFunctions/arm_dct4_init_q31.o: ../Core/Src/TransformFunctions/arm_dct4_init_q31.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F411xE -DDEBUG -c -I../Core/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../Drivers/CMSIS/Include -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Core/Src/TransformFunctions/arm_dct4_init_q31.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Core/Src/TransformFunctions/arm_dct4_q15.o: ../Core/Src/TransformFunctions/arm_dct4_q15.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F411xE -DDEBUG -c -I../Core/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../Drivers/CMSIS/Include -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Core/Src/TransformFunctions/arm_dct4_q15.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Core/Src/TransformFunctions/arm_dct4_q31.o: ../Core/Src/TransformFunctions/arm_dct4_q31.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F411xE -DDEBUG -c -I../Core/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../Drivers/CMSIS/Include -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Core/Src/TransformFunctions/arm_dct4_q31.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Core/Src/TransformFunctions/arm_rfft_f32.o: ../Core/Src/TransformFunctions/arm_rfft_f32.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F411xE -DDEBUG -c -I../Core/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../Drivers/CMSIS/Include -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Core/Src/TransformFunctions/arm_rfft_f32.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Core/Src/TransformFunctions/arm_rfft_fast_f16.o: ../Core/Src/TransformFunctions/arm_rfft_fast_f16.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F411xE -DDEBUG -c -I../Core/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../Drivers/CMSIS/Include -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Core/Src/TransformFunctions/arm_rfft_fast_f16.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Core/Src/TransformFunctions/arm_rfft_fast_f32.o: ../Core/Src/TransformFunctions/arm_rfft_fast_f32.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F411xE -DDEBUG -c -I../Core/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../Drivers/CMSIS/Include -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Core/Src/TransformFunctions/arm_rfft_fast_f32.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Core/Src/TransformFunctions/arm_rfft_fast_f64.o: ../Core/Src/TransformFunctions/arm_rfft_fast_f64.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F411xE -DDEBUG -c -I../Core/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../Drivers/CMSIS/Include -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Core/Src/TransformFunctions/arm_rfft_fast_f64.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Core/Src/TransformFunctions/arm_rfft_fast_init_f16.o: ../Core/Src/TransformFunctions/arm_rfft_fast_init_f16.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F411xE -DDEBUG -c -I../Core/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../Drivers/CMSIS/Include -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Core/Src/TransformFunctions/arm_rfft_fast_init_f16.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Core/Src/TransformFunctions/arm_rfft_fast_init_f32.o: ../Core/Src/TransformFunctions/arm_rfft_fast_init_f32.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F411xE -DDEBUG -c -I../Core/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../Drivers/CMSIS/Include -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Core/Src/TransformFunctions/arm_rfft_fast_init_f32.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Core/Src/TransformFunctions/arm_rfft_fast_init_f64.o: ../Core/Src/TransformFunctions/arm_rfft_fast_init_f64.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F411xE -DDEBUG -c -I../Core/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../Drivers/CMSIS/Include -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Core/Src/TransformFunctions/arm_rfft_fast_init_f64.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Core/Src/TransformFunctions/arm_rfft_init_f32.o: ../Core/Src/TransformFunctions/arm_rfft_init_f32.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F411xE -DDEBUG -c -I../Core/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../Drivers/CMSIS/Include -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Core/Src/TransformFunctions/arm_rfft_init_f32.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Core/Src/TransformFunctions/arm_rfft_init_q15.o: ../Core/Src/TransformFunctions/arm_rfft_init_q15.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F411xE -DDEBUG -c -I../Core/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../Drivers/CMSIS/Include -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Core/Src/TransformFunctions/arm_rfft_init_q15.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Core/Src/TransformFunctions/arm_rfft_init_q31.o: ../Core/Src/TransformFunctions/arm_rfft_init_q31.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F411xE -DDEBUG -c -I../Core/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../Drivers/CMSIS/Include -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Core/Src/TransformFunctions/arm_rfft_init_q31.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Core/Src/TransformFunctions/arm_rfft_q15.o: ../Core/Src/TransformFunctions/arm_rfft_q15.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F411xE -DDEBUG -c -I../Core/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../Drivers/CMSIS/Include -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Core/Src/TransformFunctions/arm_rfft_q15.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Core/Src/TransformFunctions/arm_rfft_q31.o: ../Core/Src/TransformFunctions/arm_rfft_q31.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F411xE -DDEBUG -c -I../Core/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../Drivers/CMSIS/Include -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Core/Src/TransformFunctions/arm_rfft_q31.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"

